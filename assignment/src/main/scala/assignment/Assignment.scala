package assignment

import java.time.LocalDateTime
import java.util.concurrent.TimeoutException
import bcrypt.AsyncBcrypt
import com.typesafe.scalalogging.StrictLogging
import store.AsyncCredentialStore
import util.Scheduler
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success}

class Assignment(bcrypt: AsyncBcrypt, credentialStore: AsyncCredentialStore)
                (implicit executionContext: ExecutionContext) extends StrictLogging {

  /**
    * проверяет пароль для пользователя
    * возвращает Future со значением false:
    *   - если пользователь не найден
    *   - если пароль не подходит к хешу
    */
  def verifyCredentials(user: String, password: String): Future[Boolean] =
    credentialStore.find(user).flatMap({
      case Some(hash) => bcrypt.verify(password, hash)
      case None => Future.successful(false)
    })

  /**
    * выполняет блок кода, только если переданы верные учетные данные
    * возвращает Future c ошибкой InvalidCredentialsException, если проверка не пройдена
    */
  def withCredentials[A](user: String, password: String)(block: => A): Future[A] =
    verifyCredentials(user, password).map({
      case true => block
      case false => throw new InvalidCredentialsException
    })

  /**
    * хеширует каждый пароль из списка и возвращает пары пароль-хеш
    */
  def hashPasswordList(passwords: Seq[String]): Future[Seq[(String, String)]] =
    Future.sequence(passwords.map(pass => bcrypt.hash(pass).map(hash => (pass,hash))))

  /**
    * проверяет все пароли из списка против хеша, и если есть подходящий - возвращает его
    * если подходит несколько - возвращается любой
    */
  def findMatchingPassword(passwords: Seq[String], hash: String): Future[Option[String]] = {
    val futures: Seq[Future[Option[String]]] =
      passwords
        .map(pass => bcrypt.verify(pass, hash)
          .map({
            case true => Some(pass)
            case _ => None
          }))
    Future.find(futures)({
      case Some(_) => true
      case _ => false
    })
      .map(o => o.flatten)
  }

  /**
    * логирует начало и окончание выполнения Future, и продолжительность выполнения
    */
  def withLogging[A](tag: String)(f: => Future[A]): Future[A] = {
    val start = System.currentTimeMillis()
    logger.trace(s"Begin task $tag at ${LocalDateTime.now}")
    val result = f
    result.onComplete { _ =>
      val end = System.currentTimeMillis()
      logger.trace(s"End task $tag at ${LocalDateTime.now} in ${end - start} ms")
    }
    result
  }

  /**
    * пытается повторно выполнить f retries раз, до первого успеха
    * если все попытки провалены, возвращает первую ошибку
    *
    * Важно: f не должна выполняться большее число раз, чем необходимо
    */
  def withRetry[A](f: => Future[A], retries: Int): Future[A] =
    retries match {
      case r if r>0 => f.recoverWith({
        case e =>
          withRetry(f, retries - 1).recover({case _ => throw e})
      })
      case _ => throw new Exception
    }

  /**
    * по истечению таймаута возвращает Future.failed с java.util.concurrent.TimeoutException
    */
  def withTimeout[A](f: => Future[A], timeout: FiniteDuration): Future[A] = {
    val p1 = Promise[A]
    Scheduler.executeAfter(timeout)(p1.failure(new TimeoutException))
    Future.firstCompletedOf(Seq(p1.future, f))
  }

  /**
    * делает то же, что и hashPasswordList, но дополнительно:
    *   - каждая попытка хеширования отдельного пароля выполняется с таймаутом
    *   - при ошибке хеширования отдельного пароля, попытка повторяется в пределах retries (свой на каждый пароль)
    *   - возвращаются все успешные результаты
    */
  def hashPasswordListReliably(passwords: Seq[String], retries: Int, timeout: FiniteDuration): Future[Seq[(String, String)]] = {
    val seq: Seq[Future[Option[(String, String)]]] =
      passwords.map((p: String) =>
        withRetry(
          withTimeout(bcrypt.hash(p), timeout),
          retries).transform({
          case Success(hash) => Success(Some((p, hash)))
          case _ => Success(None)
        }))

    Future.sequence(seq).map(s => s.collect({
      case Some(p) => p
    }))
  }
}

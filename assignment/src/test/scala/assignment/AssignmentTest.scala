package assignment

import java.util.concurrent.TimeoutException
import java.util.concurrent.atomic.AtomicInteger

import bcrypt.AsyncBcryptImpl
import com.typesafe.config.ConfigFactory
import org.scalatest._
import org.scalatest.Matchers._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class AssignmentTest extends AsyncFlatSpec {

  val config = ConfigFactory.load()
  val credentialStore = new ConfigCredentialStore(config)
  val reliableBcrypt = new AsyncBcryptImpl
  val assignment = new Assignment(reliableBcrypt, credentialStore)(ExecutionContext.global)

  import assignment._

  behavior of "verifyCredentials"

  it should "return true for valid user-password pair" in {
    verifyCredentials("winnie", "pooh").map { result =>
      result shouldBe true
    }
  }

  it should "return false if user does not exist in store" in {
    verifyCredentials("tigra", "pooh").map { result =>
      result shouldBe false
    }
  }
  it should "return false for invalid password" in {
    verifyCredentials("winnie", "poohpooh").map { result =>
      result shouldBe false
    }
  }

  behavior of "withCredentials"

  it should "execute code block if credentials are valid" in {
    withCredentials("winnie", "pooh") {
      1
    }.map(result =>
      result shouldBe 1)
  }
  it should "not execute code block if credentials are not valid" in {
    val i=new AtomicInteger(0)
    recoverToSucceededIf[InvalidCredentialsException] {
      withCredentials("winnie", "poohpooh") {
        i.incrementAndGet()
      }
    }.map(_ => i.intValue() shouldBe 0)
  }

  behavior of "hashPasswordList"

  it should "return matching password-hash pairs" in {
    val passwords: Seq[String] = Seq("pooh", "doge", "bestpassword")
    val bools: Future[Seq[Boolean]] =
      hashPasswordList(passwords)
        .flatMap(seq =>
          Future.sequence(seq.map(pair => reliableBcrypt.verify(pair._1, pair._2))))
    bools.map { result => result shouldNot contain(false) }
  }

  behavior of "findMatchingPassword"

  it should "return matching password from the list" in {
    val pass = "doge"
    val passwords: Seq[String] = Seq("pooh", pass, "bestpassword")
    reliableBcrypt.hash(pass)
      .flatMap(h => findMatchingPassword(passwords, h))
      .map(result => result shouldBe Some(pass))
  }

  it should "return None if no matching password is found" in {
    val passwords: Seq[String] = Seq("pooh", "bestpassword")
    reliableBcrypt.hash("doge")
      .flatMap(findMatchingPassword(passwords, _))
      .map(result => result shouldBe None)
  }

  behavior of "withRetry"

  it should "return result on passed future's success" in {
    val retries = 10
    val i = new AtomicInteger(0)
    withRetry({
      if (i.incrementAndGet()==3)
        Future.successful(1)
      else Future.failed(new Exception)
    }, retries)
      .map(result => {
        i.intValue() shouldBe 3
        result shouldBe 1
      })
  }

  it should "not execute more than specified number of retries" in {
    val retries = 10
    val i = new AtomicInteger(0)
    recoverToSucceededIf[Exception] {
      withRetry(
        {
          i.incrementAndGet()
          Future.failed(new Exception)
        },
        retries)
    }.map(_ => i.intValue() shouldBe retries)
  }

  it should "not execute unnecessary retries" in {
    val retries = 10
    val i = new AtomicInteger(0)
    withRetry(
      {
        i.incrementAndGet()
        Future.successful("ok")
      },
      retries).map(_ => i.intValue() shouldBe 1)
  }

  it should "return the first error, if all attempts fail" in {
    val retries = 10
    val i = new AtomicInteger(0)
    val e: Future[Exception] = recoverToExceptionIf[Exception] {
      withRetry(Future.failed(new Exception(i.incrementAndGet().toString)), retries)
    }
    e.map(ex => {
      ex.getMessage shouldBe "1"
      i.intValue() shouldBe retries
    }
    )
  }

  behavior of "withTimeout"

  it should "return result on passed future success" in {
    val timeout = 1000.millis
    withTimeout(Future.successful(1), timeout)
      .map(result => result shouldBe 1)
  }

  it should "return result on passed future failure" in {
    val timeout = 1000.millis
    recoverToSucceededIf[ArithmeticException] {
      withTimeout(Future.failed(new ArithmeticException), timeout)
    }
  }

  it should "complete on never-completing future" in {
    val timeout = 1000.millis
    recoverToSucceededIf[TimeoutException] {
      val f = Future.never
      withTimeout(f, timeout)
    }
  }

  behavior of "hashPasswordListReliably"

  it should "return password-hash pairs for successful hashing operations" in {

    val assignmentFlaky = new Assignment(new FlakyBcryptWrapper(reliableBcrypt), credentialStore)(ExecutionContext.global)
    val retries = 10
    val timeout = 1000.millis

    val passwords: Seq[String] = Seq("pooh", "doge", "bestpassword", "ultimatepass")
    val bools: Future[Seq[Boolean]] =
      assignmentFlaky.hashPasswordListReliably(passwords, retries, timeout)
        .flatMap(seq =>
          Future.sequence(seq.map(pair => reliableBcrypt.verify(pair._1, pair._2))))
    bools.map { result => result shouldNot contain(false) }
  }
}
